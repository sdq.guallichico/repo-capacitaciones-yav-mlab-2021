# Menu
- [Informacion de como acceder](#Informacion-de-como-acceder)
- [Crear repositorio](#crear-repositorio)
- [Crear grupos](#crear-grupos)
- [Crear subgrupos](#crear-subgrupos)
- [Crear issues](#crear-issues)
- [Crear labels](#crear-labels)
- [Roles que cumplen](#roles-que-cumplen)
- [Agregar miembros](#agregar-miembros)
- [Crear boards y manejos de boards](#Crear-boards-y-manejo-de-boards)

# Informacion de como acceder
- Se abre un navegador
- Ingresamos a https://gitlab.com/

- Iniciamos Sesión
- Si no tiene una cuenta en GitLab, se crea una cuenta
# Crear repositorio
<br>

- Abrimos un nuevo proyecto o creamos un proyecto en blanco
<br>
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/repo1.png
" width="60%">
</center>
<br>

- llenamos los campos de acuerdo alo que queramos y creamos el repositorio
<br
>
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/repo02.png
" width="60%">
</center>
<br>

# Crear grupos

- Para crear grupos seguimos los siguientes pasos 
<center>

<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/grupo01.png
" width="60%">
</center>
<br>

- Llenamos los campos requeridos y lo creamos 

<br>
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/grupo02.png
" width="60%">
</center>
<br>

# Crear subgrupos

- Para crear subgrupos haremos lo siguiente

<br>

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/subgrup01.png
" width="60%">
</center>

-Llenamos los campos requeridos nuevamente y creamos el subgrupo

<br> 

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/subgrup02.png
" width="60%">
</center>

<br>

# Crear issues

Crear un ISSUE Luego de esto, vamos al gitlab en el sidebar lateral izquierdo seleccionamos la opción Issue -> List y presionamos New Issue. Titulo: Un titulo claro y conciso. Descripción: Tiene que ser una descripción muy clara de que es lo que se tiene que hacer.



# Crear labels

Podemos hacerlo generando una etiqueta o TAG directamente desde gitlab. .

- 1.El procedimiento es:
- 2.Acceder al repositorio.
- 3.Acceder a Repositorio -> Tags o Etiquetas.
- 4.Crear una nueva Etiqueta con los datos completos.

<br>

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/crear-etiqueta.png
" width="60%">
</center>


# Roles que cumplen

- Acontinuacion tenrmos los roles que cumplen los invitados depende como los asignemos

<center>
<img src="
https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/roles-imagen.png
" width="60%">
</center>


# Agregar miembros
Para agregar miembros debemos dar clic en miembros en la parte inferior inquierda llenar los datos y agregar miembros.

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/crear-miembros.png
" width="60%">
</center>





# Crear boards y manejos de boards

Haga clic en el menú  con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas.
Haz clic en Crear tablero nuevo.
Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso.




<center>
<img src="
https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git-lab/imagenes/tablero.png
" width="60%">
</center>

