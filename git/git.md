# Menu
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#comandos-de-git-en-consola)
- [Clientes git](#clientes-git)
- [Clonacion de proyectos por consola y por cliente](#clonacion-de-proyectos-por-consola-y.por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

# Que es git
<p>Es un sistema de control de versiones distribuido de código abierto y gratuito diseñado para manejar todo, desde proyectos pequeños a muy grandes, con velocidad y eficiencia.</p>

# Comandos de git en consola
- git init
Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando.  Cuando agreguemos archivos y un commit, se va a crear el branch master por defecto.

<br>


<center>

<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/23bd8687153260546efed968e917a007f6c5494b/git/comandos-git/c01.png" width="60%">
</center>
- git commit -am "mensaje"
Hace commit de los archivos que han sido modificados y GIT los está siguiendo.
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/23bd8687153260546efed968e917a007f6c5494b/git/comandos-git/c01.png" width="60%">
</center>


- git commit -m "mensaje" + archivos
Hace commit a los archivos que indiquemos, de esta manera quedan guardados nuestras modificaciones.

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/c03.png
" width="60%">
</center>

- git status
Nos indica el estado del repositorio, por ejemplo cuales están modificados, cuales no están siendo seguidos por GIT, entre otras características.
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/c04.png
" width="60%">
</center>


- Git add.
Este comando añade al índice cualquier fichero nuevo o que haya sido modificado
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/c05.png
" width="60%">
</center>


# Clientes git
- Los clientes GIT o software de control de versiones se usa mayormente para gestionar código fuente. Se diseñó para el mantenimiento de las versiones de las aplicaciones cuando tienen un código fuente que contiene muchos archivos.

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/cliente-git.png
" width="60%">
</center>




# Clonacion de proyectos por consola y por cliente
- desea obtener una copia de un repositorio Git existente por ejemplo, un proyecto en el que te gustaría contribuir - el comando que necesita es git clone

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/git-clone.png
" width="60%">
</center>
aqui pondremos file y luego clone repo
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/clone-kraken.png
" width="60%">
</center>
despues llenamos los campos requeridos
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/clone-kraken2.png
" width="60%">
</center> <br><br>

# Commits por consola y por cliente kraken 
Creamos commit para poder guardar por consola cada uno de los cambios efectuados.
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/git-commit.png
" width="60%">
</center>
commits por cliente kraken
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/commit-kraken.png
" width="60%">
</center>
<br>
<br>
<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/commit-kraken-2.png
" width="60%">
</center><br> <br>



# Ramas de kraken
Para crear nos dirigimos hacia el git kraken y seleccionamos branch para crear la rama.

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/rama-crear.png
" width="60%">
</center> 
la nombramos y damos enter y se crea 
<br>
<br>

<center>
<img src="https://gitlab.com/sdq.guallichico/repo-capacitaciones-yav-mlab-2021/-/raw/master/git/comandos-git/nombrar-rama.png
" width="60%">
</center>

